#eMag Hero Game

##Description
A little game for the eMag interview.

#Requirements
* PHP 7.4 | 8

##Usage
###Start battle with eMag inputs:
```
php src/Command/EmagGame.php
```

###Battle output sample:
```
We have a new battle !!!!!!!!!!!!!!!
--------------------------------------------
Orderus properties:
Health: 84
Strength: 80
Defence: 55
Speed: 45
Luck: 26
Skills:
rapid_strike: {"chance_to_be_used":10,"number_of_extra_strikes":1}
magic_shield: {"chance_to_be_used":20,"damage_percentage":50}
--------------------------------------------
Wild beast properties:
Health: 64
Strength: 77
Defence: 54
Speed: 43
Luck: 40
***********************************
Round number: 1
Attacker: Orderus (strength: 80)
Defender: Wild beast (defence: 54; health: 64)
Strike #1 damage: 26
Orderus uses rapid_strike => He has 1 more strike(s)
Wild beast is lucky and avoids the hit
Strike #2 damage: 0
Total round damage: 26
Wild beast new health: 38
***********************************
Round number: 2
Attacker: Wild beast (strength: 77)
Defender: Orderus (defence: 55; health: 84)
Orderus uses magic_shield => The damage is decreased to 50% from normal damage: 11 instead of 22
Total round damage: 11
Orderus new health: 73
***********************************
Round number: 3
Attacker: Orderus (strength: 80)
Defender: Wild beast (defence: 54; health: 38)
Wild beast is lucky and avoids the hit
Total round damage: 0
Wild beast new health: 38
***********************************
Round number: 4
Attacker: Wild beast (strength: 77)
Defender: Orderus (defence: 55; health: 73)
Orderus uses magic_shield => The damage is decreased to 50% from normal damage: 11 instead of 22
Total round damage: 11
Orderus new health: 62
***********************************
Round number: 5
Attacker: Orderus (strength: 80)
Defender: Wild beast (defence: 54; health: 38)
Total round damage: 26
Wild beast new health: 12
***********************************
Round number: 6
Attacker: Wild beast (strength: 77)
Defender: Orderus (defence: 55; health: 62)
Total round damage: 22
Orderus new health: 40
***********************************
Round number: 7
Attacker: Orderus (strength: 80)
Defender: Wild beast (defence: 54; health: 12)
Total round damage: 26
Wild beast new health: 0
+++++++++++++++++++  We have a winner  +++++++++++++++++++
Orderus has won the battle.
```