<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;

class BaseTest extends TestCase
{
    protected function overwriteProperty($object, string $propertyName, $propertyValue)
    {
        $reflection          = new \ReflectionClass($object);
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);

        $property->setValue($object, $propertyValue);
    }

    protected function callMethod($object, string $method, array $parameters = [])
    {
        $method = $this->setPublicFunction($object, $method);
        return $method->invokeArgs($object, $parameters);
    }

    protected function setPublicFunction($object, string $method): \ReflectionMethod
    {
        $reflection = new \ReflectionClass($object);
        $method     = $reflection->getMethod($method);
        $method->setAccessible(true);

        return $method;
    }
}