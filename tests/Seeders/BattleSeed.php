<?php

namespace App\Tests\Seeders;

class BattleSeed
{
    public static int $numberOfRounds = 20;

    public static array $heroProperties = [
        'name'     => 'Orderus',
        'health'   => [
            'min' => 70,
            'max' => 100
        ],
        'strength' => [
            'min' => 70,
            'max' => 80
        ],
        'defence'  => [
            'min' => 45,
            'max' => 55
        ],
        'speed'    => [
            'min' => 40,
            'max' => 50
        ],
        'luck'     => [
            'min' => 10,
            'max' => 30
        ],
        'skills'   => [
            'rapid_strike' => [
                'chance_to_be_used'       => 10,
                'number_of_extra_strikes' => 1
            ],
            'magic_shield' => [
                'chance_to_be_used' => 20,
                'damage_percentage' => 50
            ]
        ]
    ];

    public static array $wildBeast = [
        'name'     => 'Wild beast',
        'health'   => [
            'min' => 60,
            'max' => 90
        ],
        'strength' => [
            'min' => 60,
            'max' => 90
        ],
        'defence'  => [
            'min' => 40,
            'max' => 60
        ],
        'speed'    => [
            'min' => 40,
            'max' => 60
        ],
        'luck'     => [
            'min' => 25,
            'max' => 40
        ]
    ];
}