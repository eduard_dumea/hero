<?php

namespace App\Tests\Service;

use App\Model\Character;
use App\Model\Hero;
use App\Model\MagicShieldCharacter;
use App\Model\RapidStrikeCharacter;
use App\Model\Round;
use App\Model\WildBeast;
use App\Service\CharacterService;
use App\Tests\BaseTest;
use App\Tests\Seeders\BattleSeed;

class CharacterServiceTest extends BaseTest
{
    private CharacterService $characterService;

    private Round $round;

    protected function setUp(): void
    {
        $this->characterService = new CharacterService();

        $character1 = Hero::getInstance();
        $character1->setName('name1');
        $character1->setHealth(80);
        $character1->setStrength(60);
        $character1->setDefence(50);
        $character1->setSpeed(30);
        $character1->setLuck(20);

        $character1 = new MagicShieldCharacter(
            $character1,
            [
                'chance_to_be_used' => 20,
                'damage_percentage' => 50
            ]
        );
        $character1 = new RapidStrikeCharacter(
            $character1,
            [
                'chance_to_be_used'       => 10,
                'number_of_extra_strikes' => 1
            ]
        );

        $character2 = new WildBeast();
        $character2->setName('name2');
        $character2->setHealth(85);
        $character2->setStrength(65);
        $character2->setDefence(55);
        $character2->setSpeed(35);
        $character2->setLuck(25);

        $this->round = new Round($character1, $character2);
    }

    public function testInitCharacter()
    {
        $character = new WildBeast();

        /** @var Character $character */
        $character = $this->callMethod($this->characterService, 'initCharacter', [$character, BattleSeed::$heroProperties]);

        $this->assertEquals($character->getName(), BattleSeed::$heroProperties['name']);
        $this->assertThat($character->getHealth(), $this->logicalAnd(
            $this->greaterThanOrEqual(BattleSeed::$heroProperties['health']['min']),
            $this->lessThanOrEqual(BattleSeed::$heroProperties['health']['max']))
        );
        $this->assertThat($character->getStrength(), $this->logicalAnd(
            $this->greaterThanOrEqual(BattleSeed::$heroProperties['strength']['min']),
            $this->lessThanOrEqual(BattleSeed::$heroProperties['strength']['max']))
        );
        $this->assertThat($character->getDefence(), $this->logicalAnd(
            $this->greaterThanOrEqual(BattleSeed::$heroProperties['defence']['min']),
            $this->lessThanOrEqual(BattleSeed::$heroProperties['defence']['max']))
        );
        $this->assertThat($character->getSpeed(), $this->logicalAnd(
            $this->greaterThanOrEqual(BattleSeed::$heroProperties['speed']['min']),
            $this->lessThanOrEqual(BattleSeed::$heroProperties['speed']['max']))
        );
        $this->assertThat($character->getLuck(), $this->logicalAnd(
            $this->greaterThanOrEqual(BattleSeed::$heroProperties['luck']['min']),
            $this->lessThanOrEqual(BattleSeed::$heroProperties['luck']['max']))
        );
    }

    public function testAddSkillToCharacter()
    {
        $character = new WildBeast();

        /** @var Character $character */
        $character = $this->callMethod($this->characterService, 'addSkillsToCharacter', [$character, BattleSeed::$heroProperties['skills']]);

        $this->assertEquals(2, count($character->getSkills()));

        foreach (BattleSeed::$heroProperties['skills'] as $skillName => $skillProperties) {
            $this->assertNotNull($character->getSkill($skillName));
        }
    }
}