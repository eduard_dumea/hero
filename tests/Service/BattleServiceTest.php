<?php

namespace App\Tests\Service;

use App\Service\BattleService;
use App\Tests\BaseTest;
use App\Model\{Battle, Hero, MagicShieldCharacter, RapidStrikeCharacter, Round, WildBeast};

class BattleServiceTest extends BaseTest
{
    private BattleService $battleService;

    private Battle $battle;
    private Round $round;

    protected function setUp(): void
    {
        $this->battleService = new BattleService();

        $character1 = Hero::getInstance();
        $character1->setName('name1');
        $character1->setHealth(80);
        $character1->setStrength(60);
        $character1->setDefence(50);
        $character1->setSpeed(30);
        $character1->setLuck(20);
        
        $character1 = new MagicShieldCharacter(
            $character1, 
            [
                'chance_to_be_used' => 20, 
                'damage_percentage' => 50
            ]
        );
        $character1 = new RapidStrikeCharacter(
            $character1,
            [
                'chance_to_be_used'       => 10,
                'number_of_extra_strikes' => 1
            ]
        );

        $character2 = new WildBeast();
        $character2->setName('name2');
        $character2->setHealth(85);
        $character2->setStrength(65);
        $character2->setDefence(55);
        $character2->setSpeed(35);
        $character2->setLuck(25);

        $this->battle = new Battle($character1, $character2, 20);
        $this->round = new Round($character1, $character2);
    }

    public function testCreateRoundReturnType()
    {
        /** @var Round $round */
        $round = $this->callMethod($this->battleService, 'createRound', [$this->battle]);

        $this->assertInstanceOf(Round::class, $round);
    }

    public function testCreateRoundCharacter1IsFirst()
    {
        $this->battle->getCharacter1()->setSpeed(40);
        $this->battle->getCharacter2()->setSpeed(30);

        /** @var Round $round */
        $round = $this->callMethod($this->battleService, 'createRound', [$this->battle]);

        self::assertEquals($round->getAttacker()->getName(), $this->battle->getCharacter1()->getName());
    }

    public function testCreateRoundCharacter2IsFirst()
    {
        $this->battle->getCharacter1()->setSpeed(30);
        $this->battle->getCharacter2()->setSpeed(40);

        /** @var Round $round */
        $round = $this->callMethod($this->battleService, 'createRound', [$this->battle]);

        self::assertEquals($round->getAttacker()->getName(), $this->battle->getCharacter2()->getName());
    }

    public function testIsCharacter1FirstAttackerWhenCharacter1IsFaster()
    {
        $this->battle->getCharacter1()->setSpeed(20);
        $this->battle->getCharacter2()->setSpeed(10);

        $result = $this->callMethod($this->battleService, 'isCharacter1FirstAttacker', [$this->battle]);

        self::assertTrue($result);
    }

    public function testIsCharacter1FirstAttackerWhenCharacter2IsFaster()
    {
        $this->battle->getCharacter1()->setSpeed(30);
        $this->battle->getCharacter2()->setSpeed(40);

        $result = $this->callMethod($this->battleService, 'isCharacter1FirstAttacker', [$this->battle]);

        self::assertFalse($result);
    }

    public function testIsCharacter1FirstAttackerWhenCharacter1IsLuckier()
    {
        $this->battle->getCharacter1()->setSpeed(30);
        $this->battle->getCharacter2()->setSpeed(30);

        $this->battle->getCharacter1()->setLuck(40);
        $this->battle->getCharacter2()->setLuck(30);

        $result = $this->callMethod($this->battleService, 'isCharacter1FirstAttacker', [$this->battle]);

        self::assertTrue($result);
    }

    public function testIsCharacter1FirstAttackerWhenCharacter2IsLuckier()
    {
        $this->battle->getCharacter1()->setSpeed(30);
        $this->battle->getCharacter2()->setSpeed(30);

        $this->battle->getCharacter1()->setLuck(40);
        $this->battle->getCharacter2()->setLuck(50);

        $result = $this->callMethod($this->battleService, 'isCharacter1FirstAttacker', [$this->battle]);

        self::assertFalse($result);
    }

    public function testIsGameOverMaximumRoundsNumberReached()
    {
        $this->battle->setMaximumNumberOfRounds(20);
        $this->round->setRoundNumber(21);
        $result = $this->callMethod($this->battleService, 'isGameOver', [$this->battle, $this->round]);

        self::assertTrue($result);
    }

    public function testIsGameOverMaximumRoundsNumberNotReached()
    {
        $this->battle->setMaximumNumberOfRounds(22);
        $this->round->setRoundNumber(21);
        $result = $this->callMethod($this->battleService, 'isGameOver', [$this->battle, $this->round]);

        self::assertFalse($result);
    }

    public function testIsGameOverCharacter1IsDead()
    {
        $this->battle->setMaximumNumberOfRounds(22);
        $this->round->setRoundNumber(21);

        $this->battle->getCharacter1()->setHealth(0);
        $this->battle->getCharacter2()->setHealth(10);
        $result = $this->callMethod($this->battleService, 'isGameOver', [$this->battle, $this->round]);

        self::assertTrue($result);
    }

    public function testIsGameOverCharacter2IsDead()
    {
        $this->battle->setMaximumNumberOfRounds(22);
        $this->round->setRoundNumber(21);

        $this->battle->getCharacter1()->setHealth(10);
        $this->battle->getCharacter2()->setHealth(0);
        $result = $this->callMethod($this->battleService, 'isGameOver', [$this->battle, $this->round]);

        self::assertTrue($result);
    }
}