<?php

namespace App\Tests\Service;

use App\Model\Hero;
use App\Model\WildBeast;
use App\Service\BattleService;
use App\Service\CharacterService;
use App\Service\EmagGameService;
use App\Tests\BaseTest;

class EmagGameServiceTest extends BaseTest
{
    private EmagGameService $emagGameService;

    private $mockBattleService;
    private $mockCharacterService;

    protected function setUp(): void
    {
        $this->mockBattleService = $this->createMock(BattleService::class);
        $this->mockCharacterService = $this->createMock(CharacterService::class);

        $this->emagGameService = new EmagGameService();
    }

    public function testGame()
    {
        $this->mockBattleService
            ->expects($this->once())
            ->method('battle');

        $this->overwriteProperty($this->emagGameService, 'battleService', $this->mockBattleService);

        $this->emagGameService->game();
    }

    public function testInitHero()
    {
        $hero = Hero::getInstance();
        $this->mockCharacterService
            ->expects($this->once())
            ->method('initCharacter')
            ->willReturn($hero);

        $this->overwriteProperty($this->emagGameService, 'characterService', $this->mockCharacterService);


        $character = $this->callMethod($this->emagGameService, 'initHero', []);

        self::assertInstanceOf(Hero::class, $character);
    }

    public function testInitWildBeast()
    {
        $wildBeast = new WildBeast();
        $this->mockCharacterService
            ->expects($this->once())
            ->method('initCharacter')
            ->willReturn($wildBeast);

        $this->overwriteProperty($this->emagGameService, 'characterService', $this->mockCharacterService);


        $character = $this->callMethod($this->emagGameService, 'initHero', []);

        self::assertInstanceOf(WildBeast::class, $character);
    }
}