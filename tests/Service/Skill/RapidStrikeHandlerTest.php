<?php

namespace App\Tests\Service\Skill;

use App\Model\Hero;
use App\Model\Round;
use App\Model\Skill;
use App\Model\WildBeast;
use App\Service\LogService;
use App\Service\RoundService;
use App\Service\Skill\RapidStrikeHandler;
use App\Tests\BaseTest;

class RapidStrikeHandlerTest extends BaseTest
{
    private RapidStrikeHandler $rapidStrikeHandler;

    private Round $round;
    private Skill $skill;

    private $mockRoundService;
    private $mockLogService;

    protected function setUp(): void
    {
        $this->rapidStrikeHandler = new RapidStrikeHandler();

        $this->mockRoundService = $this->createMock(RoundService::class);
        $this->mockLogService = $this->createMock(LogService::class);

        $this->overwriteProperty($this->rapidStrikeHandler, 'roundService', $this->mockRoundService);
        $this->overwriteProperty($this->rapidStrikeHandler, 'logService', $this->mockLogService);

        $character1 = Hero::getInstance();
        $character2 = new WildBeast();

        $this->round = new Round($character1, $character2);
        $this->skill = new Skill('', []);
    }

    public function testCanUseSkillTrue()
    {
        $this->skill->setProperties(['chance_to_be_used' => 100]);
        $result = $this->callMethod($this->rapidStrikeHandler, 'canUseSkill', [$this->skill]);

        $this->assertTrue($result);
    }

    public function testCanUseSkillWhenChanceIs0()
    {
        $this->skill->setProperties(['chance_to_be_used' => 0]);
        $result = $this->callMethod($this->rapidStrikeHandler, 'canUseSkill', [$this->skill]);

        $this->assertFalse($result);
    }

    public function testCanUseSkillWhenSkillIsNull()
    {
        $result = $this->callMethod($this->rapidStrikeHandler, 'canUseSkill', [null]);

        $this->assertFalse($result);
    }

    public function testUseSkillWhenExtraStrikesIs1()
    {
        $this->round->setDamage(80);
        $this->skill->setProperties(['number_of_extra_strikes' => 1]);

        $this->mockRoundService
            ->expects($this->once())
            ->method('computeDamage')->willReturnCallback(function (){$this->round->setDamage(30);});


        $this->callMethod($this->rapidStrikeHandler, 'useSkill', [$this->round, $this->skill]);

        $this->assertEquals(110, $this->round->getDamage());
    }

    public function testUseSkillWhenExtraStrikesIs2()
    {
        $this->round->setDamage(80);
        $this->skill->setProperties(['number_of_extra_strikes' => 2]);

        $this->mockRoundService
            ->expects($this->exactly(2))
            ->method('computeDamage')->willReturnCallback(function (){$this->round->setDamage(30);});


        $this->callMethod($this->rapidStrikeHandler, 'useSkill', [$this->round, $this->skill]);

        $this->assertEquals(140, $this->round->getDamage());
    }
}