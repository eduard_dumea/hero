<?php

namespace App\Tests\Service\Skill;

use App\Model\Hero;
use App\Model\Round;
use App\Model\Skill;
use App\Model\WildBeast;
use App\Service\LogService;
use App\Service\Skill\MagicShieldHandler;
use App\Tests\BaseTest;

class MagicShieldHandlerTest extends BaseTest
{
    private MagicShieldHandler $magicShieldHandler;

    private $mockLogService;

    private Round $round;
    private Skill $skill;

    protected function setUp(): void
    {
        $this->magicShieldHandler = new MagicShieldHandler();

        $this->mockLogService = $this->createMock(LogService::class);

        $this->overwriteProperty($this->magicShieldHandler, 'logService', $this->mockLogService);

        $character1 = Hero::getInstance();
        $character2 = new WildBeast();

        $this->round = new Round($character1, $character2);
        $this->skill = new Skill('', []);
    }

    public function testCanUseSkillTrue()
    {
        $this->round->setDamage(100);
        $this->skill->setProperties(['chance_to_be_used' => 100]);
        $result = $this->callMethod($this->magicShieldHandler, 'canUseSkill', [$this->round, $this->skill]);

        $this->assertTrue($result);
    }

    public function testCanUseSkillWhenDamageIs0()
    {
        $this->round->setDamage(0);
        $this->skill->setProperties(['chance_to_be_used' => 100]);
        $result = $this->callMethod($this->magicShieldHandler, 'canUseSkill', [$this->round, $this->skill]);

        $this->assertFalse($result);
    }

    public function testCanUseSkillWhenChanceIs0()
    {
        $this->round->setDamage(100);
        $this->skill->setProperties(['chance_to_be_used' => 0]);
        $result = $this->callMethod($this->magicShieldHandler, 'canUseSkill', [$this->round, $this->skill]);

        $this->assertFalse($result);
    }

    public function testCanUseSkillWhenSkillIsNull()
    {
        $result = $this->callMethod($this->magicShieldHandler, 'canUseSkill', [$this->round, null]);

        $this->assertFalse($result);
    }

    public function testUseSkillWhenDamagePercentageIs50()
    {
        $this->round->setDamage(80);
        $this->skill->setProperties(['damage_percentage' => 50]);
        $this->callMethod($this->magicShieldHandler, 'useSkill', [$this->round, $this->skill]);

        $this->assertEquals(40, $this->round->getDamage());
    }

    public function testUseSkillWhenDamagePercentageIs24()
    {
        $this->round->setDamage(80);
        $this->skill->setProperties(['damage_percentage' => 24]);
        $this->callMethod($this->magicShieldHandler, 'useSkill', [$this->round, $this->skill]);

        $this->assertEquals(19, $this->round->getDamage());
    }
}