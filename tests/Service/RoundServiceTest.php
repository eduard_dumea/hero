<?php

namespace App\Tests\Tests\Service;

use App\Model\Hero;
use App\Model\Round;
use App\Model\WildBeast;
use App\Service\LogService;
use App\Service\RoundService;
use App\Service\Skill\ChainOfResponsibility;
use App\Tests\BaseTest;

class RoundServiceTest extends BaseTest
{
    private RoundService $roundService;

    private Round $round;
    private $mockChainOfResponsibility;
    private $mockLogService;

    protected function setUp(): void
    {
        $this->roundService = new RoundService();
        $this->mockChainOfResponsibility = $this->createMock(ChainOfResponsibility::class);
        $this->mockLogService = $this->createMock(LogService::class);

        $this->overwriteProperty($this->roundService, 'chainOfResponsibility', $this->mockChainOfResponsibility);
        $this->overwriteProperty($this->roundService, 'logService', $this->mockLogService);

        $character1 = Hero::getInstance();
        $character2 = new WildBeast();

        $this->round = new Round($character1, $character2);
    }

    public function testPlayRound()
    {
        $this->mockChainOfResponsibility
            ->expects($this->once())
            ->method('useSkills');

        $this->overwriteProperty($this->roundService, 'chainOfResponsibility', $this->mockChainOfResponsibility);

        $this->round->getAttacker()->setStrength(20);
        $this->round->getDefender()->setDefence(15);
        $this->round->getDefender()->setLuck(0);



        $this->callMethod($this->roundService, 'playRound', [$this->round]);

        self::assertEquals(5, $this->round->getDamage());
    }

    public function testComputeDamageDefenderIsLucky()
    {
        $this->round->getAttacker()->setStrength(20);
        $this->round->getDefender()->setDefence(15);
        $this->round->getDefender()->setLuck(100);

        $this->callMethod($this->roundService, 'computeDamage', [$this->round]);

        self::assertEquals(0, $this->round->getDamage());
    }

    public function testComputeDamageDefenderIsUnlucky()
    {
        $this->round->getAttacker()->setStrength(45);
        $this->round->getDefender()->setDefence(20);
        $this->round->getDefender()->setLuck(0);

        $this->callMethod($this->roundService, 'computeDamage', [$this->round]);

        self::assertEquals(25, $this->round->getDamage());
    }

    public function testSwitchRoles()
    {
        $roundCopy = clone($this->round);
        $this->callMethod($this->roundService, 'switchRoles', [$this->round]);

        self::assertEquals($roundCopy->getAttacker(), $this->round->getDefender());
        self::assertEquals($roundCopy->getDefender(), $this->round->getAttacker());
    }

    public function testIncreaseRoundNumber()
    {
        $this->round->setRoundNumber(10);
        $this->callMethod($this->roundService, 'increaseRoundNumber', [$this->round]);

        self::assertEquals(11, $this->round->getRoundNumber());
    }

    public function testIsDefenderLuckyTrue()
    {
        $this->round->getDefender()->setLuck(100);
        $result  = $this->callMethod($this->roundService, 'isDefenderLucky', [$this->round]);

        $this->assertTrue($result);
    }

    public function testIsDefenderLuckyFalse()
    {
        $this->round->getDefender()->setLuck(0);
        $result  = $this->callMethod($this->roundService, 'isDefenderLucky', [$this->round]);

        $this->assertFalse($result);
    }
}