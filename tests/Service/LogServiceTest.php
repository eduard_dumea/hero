<?php

namespace App\Tests\Service;

use App\Service\LogService;
use App\Tests\BaseTest;

class LogServiceTest extends BaseTest
{
    private LogService $logService;

    protected function setUp(): void
    {
        $this->logService = new LogService();
    }

    public function testLog()
    {
        $message = 'test message';
        $this->expectOutputString($message . "\r\n");
        $this->logService->log($message);
    }
}