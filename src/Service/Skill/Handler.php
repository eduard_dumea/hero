<?php

namespace App\Service\Skill;

use App\Model\Round;

interface Handler
{
    public function setNext(Handler $handler): Handler;

    public function handle(Round $round): Round;
}