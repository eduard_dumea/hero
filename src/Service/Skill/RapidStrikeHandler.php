<?php

namespace App\Service\Skill;

use App\Model\{Round, Skill};

class RapidStrikeHandler extends AbstractHandler
{
    public function handle(Round $round): Round
    {
        $skill = $round->getAttacker()->getSkill(Skill::RAPID_STRIKE);

        if ($this->canUseSkill($skill)) {
            $this->useSkill($round, $skill);
        }

        return parent::handle($round);
    }

    private function canUseSkill(?Skill $skill): bool
    {
        return $skill && mt_rand(1, 100) <= $skill->getProperties()['chance_to_be_used'];
    }

    private function useSkill(Round $round, Skill $skill): void
    {
        $totalDamage = $round->getDamage();

        $this->logService->log('Strike #1 damage: ' . $totalDamage);

        $this->logService->log($round->getAttacker()->getName() . ' uses ' . Skill::RAPID_STRIKE .
            ' => He has ' . $skill->getProperties()['number_of_extra_strikes'] . ' more strike(s)');


        for ($i = 1; $i <= $skill->getProperties()['number_of_extra_strikes']; $i++) {
            $this->roundService->computeDamage($round);

            $strikeNumber = $i + 1;
            $this->logService->log("Strike #$strikeNumber damage: " . $round->getDamage());
            $totalDamage += $round->getDamage();
        }

        $round->setDamage($totalDamage);
    }
}