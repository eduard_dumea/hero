<?php

namespace App\Service\Skill;

use App\Model\{Round, Skill};

class MagicShieldHandler extends AbstractHandler
{
    public function handle(Round $round): Round
    {
        $skill = $round->getDefender()->getSkill(Skill::MAGIC_SHIELD);

        if ($this->canUseSkill($round, $skill)) {
            $this->useSkill($round, $skill);
        }

        return parent::handle($round);
    }

    private function canUseSkill(Round $round, ?Skill $skill): bool
    {
        return $round->getDamage() && $skill && mt_rand(1, 100) <= $skill->getProperties()['chance_to_be_used'];
    }

    private function useSkill(Round $round, Skill $skill): void
    {
        $oldDamage = $round->getDamage();
        $newDamage = (int)($skill->getProperties()['damage_percentage'] / 100 * $round->getDamage());
        $round->setDamage($newDamage);

        $this->logService->log($round->getDefender()->getName() . ' uses ' . Skill::MAGIC_SHIELD .
            ' => The damage is decreased to ' . $skill->getProperties()['damage_percentage'] .
            "% from normal damage: $newDamage instead of $oldDamage");
    }
}