<?php

namespace App\Service\Skill;

use App\Model\Round;
use App\Service\CharacterService;
use App\Service\LogService;
use App\Service\RoundService;

abstract class AbstractHandler implements Handler
{
    protected LogService $logService;
    protected CharacterService $characterService;
    protected RoundService $roundService;

    private ?Handler $nextHandler = null;

    public function __construct()
    {
        $this->logService       = new LogService();
        $this->characterService = new CharacterService();
        $this->roundService     = new RoundService();
    }

    public function setNext(Handler $handler): Handler
    {
        $this->nextHandler = $handler;
        return $handler;
    }

    public function handle(Round $round): Round
    {
        if ($this->nextHandler) {
            return $this->nextHandler->handle($round);
        }

        return $round;
    }
}