<?php

namespace App\Service\Skill;

use App\Model\Round;

class ChainOfResponsibility
{
    public function useSkills(Round $round)
    {
        $magicShield = new MagicShieldHandler();
        $rapidStrike = new RapidStrikeHandler();

        $magicShield->setNext($rapidStrike);

        $magicShield->handle($round);
    }
}