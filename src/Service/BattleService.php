<?php

namespace App\Service;

use App\Model\{Battle, Character, Round};

class BattleService
{
    private LogService $logService;
    private RoundService $roundService;
    private CharacterService $characterService;

    public function __construct()
    {
        $this->roundService     = new RoundService();
        $this->logService       = new LogService();
        $this->characterService = new CharacterService();
    }

    public function battle(Battle $battle): void
    {
        $this->startBattle($battle);
        $this->playRounds($battle);
        $this->endBattle($battle);
    }

    private function createRound(Battle $battle): Round
    {
        if ($this->isCharacter1FirstAttacker($battle)) {
            $round = new Round($battle->getCharacter1(), $battle->getCharacter2());
        } else {
            $round = new Round($battle->getCharacter2(), $battle->getCharacter1());
        }
        return $round;
    }

    private function isCharacter1FirstAttacker(Battle $battle): bool
    {
        //todo instructions are unclear when both characters have the same speed and luck

        return $battle->getCharacter1()->getSpeed() > $battle->getCharacter2()->getSpeed() ||
            (
                $battle->getCharacter1()->getSpeed() == $battle->getCharacter2()->getSpeed() &&
                $battle->getCharacter1()->getLuck() > $battle->getCharacter2()->getLuck()
            );
    }

    private function isGameOver(Battle $battle, Round $round): bool
    {
        return $round->getRoundNumber() > $battle->getMaximumNumberOfRounds() ||
            $battle->getCharacter1()->isDead() ||
            $battle->getCharacter2()->isDead();
    }

    private function startBattle(Battle $battle): void
    {
        $this->logService->log('We have a new battle !!!!!!!!!!!!!!!');
        $this->characterService->logCharacter($battle->getCharacter1());
        $this->characterService->logCharacter($battle->getCharacter2());
    }

    private function declareWinner(Character $winner): void
    {
        $this->logService->log('+++++++++++++++++++  We have a winner  +++++++++++++++++++');
        $this->logService->log($winner->getName() . ' has won the battle.');
    }

    private function playRounds(Battle $battle): void
    {
        $round = $this->createRound($battle);

        while (!$this->isGameOver($battle, $round)) {
            $this->roundService->play($round);
        }
    }

    private function endBattle(Battle $battle): void
    {
        if ($battle->getCharacter1()->isDead()) {
            $this->declareWinner($battle->getCharacter2());
        } elseif ($battle->getCharacter2()->isDead()) {
            $this->declareWinner($battle->getCharacter1());
        } else {
            $this->logService->log('+++++++++++++++++  Draw  ++++++++++++++++++++');
        }
    }
}