<?php

namespace App\Service;

use App\Model\{Character, MagicShieldCharacter, RapidStrikeCharacter, Skill};

class CharacterService
{
    private LogService $logService;

    public function __construct()
    {
        $this->logService = new LogService();
    }

    public function initCharacter(Character $character, array $properties): Character
    {
        $character->setName($properties['name']);
        $character->setHealth(mt_rand($properties['health']['min'], $properties['health']['max']));
        $character->setStrength(mt_rand($properties['strength']['min'], $properties['strength']['max']));
        $character->setDefence(mt_rand($properties['defence']['min'], $properties['defence']['max']));
        $character->setSpeed(mt_rand($properties['speed']['min'], $properties['speed']['max']));
        $character->setLuck(mt_rand($properties['luck']['min'], $properties['luck']['max']));

        if (!empty($properties['skills'])) {
            $character = $this->addSkillsToCharacter($character, $properties['skills']);
        }

        return $character;
    }

    public function logCharacter(Character $character): void
    {
        $this->logService->log('--------------------------------------------');
        $this->logService->log($character->getName() . ' properties: ');
        $this->logService->log('Health: ' . $character->getHealth());
        $this->logService->log('Strength: ' . $character->getStrength());
        $this->logService->log('Defence: ' . $character->getDefence());
        $this->logService->log('Speed: ' . $character->getSpeed());
        $this->logService->log('Luck: ' . $character->getLuck());

        $this->logSkills($character);
    }

    private function logSkills(Character $character): void
    {
        if ($character->getSkills()) {
            $this->logService->log('Skills: ');
            /** @var Skill $skill */
            foreach ($character->getSkills() as $skill) {
                $this->logService->log($skill->getName() . ': ' . json_encode($skill->getProperties()));
            }
        }
    }

    private function addSkillsToCharacter(Character $character, array $skills): Character
    {
        foreach ($skills as $skillName => $skillProperties) {
            switch ($skillName) {
                case Skill::MAGIC_SHIELD:
                    $character = new MagicShieldCharacter($character, $skillProperties);
                    break;
                case Skill::RAPID_STRIKE:
                    $character = new RapidStrikeCharacter($character, $skillProperties);
                    break;
                default:
                    $character->addSkill(new Skill($skillName, $skillProperties));
                    break;
            }
        }

        return $character;
    }
}