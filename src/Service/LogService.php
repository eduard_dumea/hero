<?php

namespace App\Service;

class LogService
{
    public function log(string $message): void
    {
        print($message . "\r\n");
    }
}