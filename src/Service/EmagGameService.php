<?php

namespace App\Service;

use App\Model\{Battle, Character, Hero, WildBeast};
use App\Seeders\EmagGame;

class EmagGameService
{
    private CharacterService $characterService;
    private BattleService $battleService;

    public function __construct()
    {
        $this->battleService    = new BattleService();
        $this->characterService = new CharacterService();
    }

    public function game()
    {
        $hero      = $this->initHero();
        $wildBeast = $this->initWildBeast();

        $battle = new Battle($hero, $wildBeast, EmagGame::$numberOfRounds);

        $this->battleService->battle($battle);
    }

    private function initHero(): Character
    {
        $hero = Hero::getInstance();

        return $this->characterService->initCharacter($hero, EmagGame::$heroProperties);
    }

    private function initWildBeast(): Character
    {
        $wildBeast = new WildBeast();

        return $this->characterService->initCharacter($wildBeast, EmagGame::$wildBeast);
    }
}