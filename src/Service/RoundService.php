<?php

namespace App\Service;

use App\Model\Round;
use App\Service\Skill\ChainOfResponsibility;

class RoundService
{
    private LogService $logService;
    private ChainOfResponsibility $chainOfResponsibility;

    public function __construct()
    {
        $this->logService            = new LogService();
        $this->chainOfResponsibility = new ChainOfResponsibility();
    }

    public function play(Round $round): void
    {
        $this->startRound($round);
        $this->playRound($round);
        $this->endRound($round);
    }

    private function startRound(Round $round): void
    {
        $this->logService->log('***********************************');
        $this->logService->log("Round number: " . $round->getRoundNumber());
        $this->logService->log("Attacker: " . $round->getAttacker()->getName() .
            ' (strength: ' . $round->getAttacker()->getStrength() . ')');
        $this->logService->log("Defender: " . $round->getDefender()->getName() .
            ' (defence: ' . $round->getDefender()->getDefence() .
            '; health: ' . $round->getDefender()->getHealth() . ')');
    }

    private function playRound(Round $round): void
    {
        $this->computeDamage($round);
        $this->chainOfResponsibility->useSkills($round);

        $this->logService->log("Total round damage: " . $round->getDamage());
        $round->getDefender()->setHealth($this->getDefenderNewHealth($round));
    }

    private function endRound(Round $round): void
    {
        $this->logService->log($round->getDefender()->getName() . ' new health: ' . $round->getDefender()->getHealth());
        $this->switchRoles($round);
        $this->increaseRoundNumber($round);
    }

    public function computeDamage(Round $round): void
    {
        if ($this->isDefenderLucky($round)) {
            $this->logService->log($round->getDefender()->getName() . " is lucky and avoids the hit");
            $damage = 0;
        } else {
            $damage = max(0, $round->getAttacker()->getStrength() - $round->getDefender()->getDefence());
        }

        $round->setDamage($damage);
    }

    public function switchRoles(Round $round): void
    {
        $attacker = $round->getAttacker();
        $round->setAttacker($round->getDefender());
        $round->setDefender($attacker);
    }

    public function increaseRoundNumber(Round $round): void
    {
        $round->setRoundNumber($round->getRoundNumber() + 1);
    }

    private function isDefenderLucky(Round $round): bool
    {
        return mt_rand(1, 100) <= $round->getDefender()->getLuck();
    }

    private function getDefenderNewHealth(Round $round): int
    {
        return max(0,$round->getDefender()->getHealth() - $round->getDamage());
    }
}