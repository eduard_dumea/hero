<?php

namespace App\Model;

class RapidStrikeCharacter extends CharacterDecorator
{
    public function __construct(Character $character, array $skillProperties)
    {
        parent::__construct($character);
        $this->setSkills([new Skill(Skill::RAPID_STRIKE, $skillProperties)]);
    }
}