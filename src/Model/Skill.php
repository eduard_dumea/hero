<?php

namespace App\Model;

class Skill
{
    public const RAPID_STRIKE = 'rapid_strike';
    public const MAGIC_SHIELD = 'magic_shield';

    protected string $name = '';
    protected array $properties = [];

    public function __construct(string $name, array $properties = [])
    {
        $this->name       = $name;
        $this->properties = $properties;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Skill
    {
        $this->name = $name;
        return $this;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }

    public function setProperties(array $properties): Skill
    {
        $this->properties = $properties;
        return $this;
    }
}