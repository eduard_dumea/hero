<?php

namespace App\Model;

class MagicShieldCharacter extends CharacterDecorator
{
    public function __construct(Character $character, array $skillProperties)
    {
        parent::__construct($character);
        $this->setSkills([new Skill(Skill::MAGIC_SHIELD, $skillProperties)]);
    }
}