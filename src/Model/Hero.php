<?php

namespace App\Model;

class Hero extends Character
{
    private static ?Hero $hero = null;

    private function __construct()
    {
    }

    public static function getInstance(): Hero
    {
        if (self::$hero === null) {
            self::$hero = new Hero();
        }

        return self::$hero;
    }
}