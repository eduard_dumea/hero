<?php

namespace App\Model;

class Battle
{
    protected Character $character1;
    protected Character $character2;

    protected int $maximumNumberOfRounds = 0;

    public function __construct(Character $character1, Character $character2, int $maximumNumberOfRounds)
    {
        $this->character1            = $character1;
        $this->character2            = $character2;
        $this->maximumNumberOfRounds = $maximumNumberOfRounds;
    }

    public function getCharacter1(): Character
    {
        return $this->character1;
    }

    public function setCharacter1(Character $character1): Battle
    {
        $this->character1 = $character1;
        return $this;
    }

    public function getCharacter2(): Character
    {
        return $this->character2;
    }

    public function setCharacter2(Character $character2): Battle
    {
        $this->character2 = $character2;
        return $this;
    }

    public function getMaximumNumberOfRounds(): int
    {
        return $this->maximumNumberOfRounds;
    }

    public function setMaximumNumberOfRounds(int $maximumNumberOfRounds): Battle
    {
        $this->maximumNumberOfRounds = $maximumNumberOfRounds;
        return $this;
    }
}