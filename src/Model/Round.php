<?php

namespace App\Model;

class Round
{
    protected Character $attacker;
    protected Character $defender;

    protected int $roundNumber = 1;

    protected int $damage = 0;

    public function __construct(Character $attacker, Character $defender)
    {
        $this->attacker = $attacker;
        $this->defender = $defender;
    }

    public function getAttacker(): Character
    {
        return $this->attacker;
    }

    public function setAttacker(Character $attacker): Round
    {
        $this->attacker = $attacker;
        return $this;
    }

    public function getDefender(): Character
    {
        return $this->defender;
    }

    public function setDefender(Character $defender): Round
    {
        $this->defender = $defender;
        return $this;
    }

    public function getRoundNumber(): int
    {
        return $this->roundNumber;
    }

    public function setRoundNumber(int $roundNumber): Round
    {
        $this->roundNumber = $roundNumber;
        return $this;
    }

    public function getDamage(): int
    {
        return $this->damage;
    }

    public function setDamage(int $damage): Round
    {
        $this->damage = $damage;
        return $this;
    }


}