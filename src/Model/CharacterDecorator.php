<?php

namespace App\Model;

abstract class CharacterDecorator extends Character
{
    protected Character $character;

    protected int $health = 0;
    protected int $strength = 0;
    protected int $defence = 0;
    protected int $speed = 0;
    protected int $luck = 0;
    protected array $skills = [];

    public function __construct(Character $character)
    {
        $this->character = $character;
    }

    public function getName(): string
    {
        return $this->character->getName();
    }

    public function setHealth(int $health): Character
    {
        return $this->character->setHealth($health);
    }

    public function getHealth(): int
    {
        return $this->character->getHealth();
    }

    public function getStrength(): int
    {
        return $this->character->getStrength() + $this->strength;
    }

    public function setStrength(int $strength): Character
    {
        return $this->character->setStrength($strength);
    }

    public function getDefence(): int
    {
        return $this->character->getDefence() + $this->defence;
    }

    public function setDefence(int $defence): Character
    {
        return $this->character->setDefence($defence);
    }

    public function getSpeed(): int
    {
        return $this->character->getSpeed() + $this->speed;
    }

    public function setSpeed(int $speed): Character
    {
        return $this->character->setSpeed($speed);
    }

    public function getLuck(): int
    {
        return $this->character->getLuck() + $this->luck;
    }

    public function setLuck(int $luck): Character
    {
        return $this->character->setLuck($luck);
    }

    public function getSkills(): array
    {
        return array_merge($this->character->getSkills(), $this->skills);
    }
}