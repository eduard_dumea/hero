<?php

namespace App\Model;

abstract class Character
{
    protected string $name = '';
    protected int $health = 0;
    protected int $strength = 0;
    protected int $defence = 0;
    protected int $speed = 0;
    protected int $luck = 0;
    protected array $skills = [];

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Character
    {
        $this->name = $name;
        return $this;
    }

    public function getHealth(): int
    {
        return $this->health;
    }

    public function setHealth(int $health): Character
    {
        $this->health = $health;
        return $this;
    }

    public function getStrength(): int
    {
        return $this->strength;
    }

    public function setStrength(int $strength): Character
    {
        $this->strength = $strength;
        return $this;
    }

    public function getDefence(): int
    {
        return $this->defence;
    }

    public function setDefence(int $defence): Character
    {
        $this->defence = $defence;
        return $this;
    }

    public function getSpeed(): int
    {
        return $this->speed;
    }

    public function setSpeed(int $speed): Character
    {
        $this->speed = $speed;
        return $this;
    }

    public function getLuck(): int
    {
        return $this->luck;
    }

    public function setLuck(int $luck): Character
    {
        $this->luck = $luck;
        return $this;
    }

    public function addSkill(Skill $skill): Character
    {
        array_push($this->skills, $skill);
        return $this;
    }

    public function getSkills(): array
    {
        return $this->skills;
    }

    public function getSkill(string $skillName): ?Skill
    {
        /** @var Skill $skill */
        foreach ($this->getSkills() as $skill) {
            if ($skill->getName() == $skillName) {
                return $skill;
            }
        }

        return null;
    }

    public function setSkills(array $skills): Character
    {
        $this->skills = $skills;
        return $this;
    }

    public function isDead(): bool
    {
        return $this->getHealth() <= 0;
    }
}